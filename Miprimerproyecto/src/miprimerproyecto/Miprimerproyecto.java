/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miprimerproyecto;
import java.util.Scanner;

/**
 *
 * @author Karen Perez
 */
public class Miprimerproyecto {
    static void encabezado (){
        System.out.print(" ██╗    ██╗  ███████╗ ███████╗  ███████╗ ██╗    ██╗███████╗ ██████╗\n");
        System.out.print(" ██║    ██║  ██╔══██╗ ██╔════╝  ██╔════╝ ██║    ██║██╔════╝██╔════╝\n");
        System.out.print(" ██║    ██║  ███████║ ██║          █████╗    ██║    ██║███████╗██║     \n");
        System.out.print(" ██║    ██║  ██╔══██║ ██║          ██╔══╝    ██║    ██║╚════██║██║     \n");
        System.out.println("╚██████╔╝  ██║   ██║╚██████╗   ██║         ██║    ██║███████║╚██████╗");
        System.out.println("                       Universidad Autonoma de Campeche");
        System.out.println("                           Facultad de ingenieria");
        System.out.println("                    Ingenieria en Sistemas Computacionales");
    }
    
    static void separador (boolean tipo){
        if(tipo){
            System.out.println("_____________________________________________________________________________________");
        } else {
        System.out.println("======================================================================================");
        System.out.println("                             Karen Estefania Perez Perez");
        }
    
    }
    
    static void cuerpo (boolean tipo){
        if (tipo){
        System.out.println("                               EL CORONAVIRUS");
        System.out.println("1.- Informacion general");
        System.out.println("2.-Efectos/Sintomas");
        System.out.println("3.-Poblacion vulnerable");
        System.out.println("4.-3 Estados infectados");
        System.out.println("5.-Medidas de higiene");
        } else{
        System.out.print("");
        }
    }
    
    
    /**
     * @param args the command line arguments
     */
    static void codigo (){
     //Codigo del menu
        Scanner sc = new Scanner (System.in);
        double menu;
        
        System.out.print("Introduzca un numero del 1 al 5 de la informacion que desee saber: ");
        menu = sc.nextDouble();
        
        separador(true);
        
        if(menu==1){
        
        System.out.println("1.- Informacion general");
        System.out.println("   ");
        System.out.println("La COVID-19 es una enfermedad infecciosa causada por un nuevo virus");
        System.out.println("que no habia sido detectado en humanos hasta la fecha.");
        System.out.println("El virus es una afeccion respiratoria que se puede");
        System.out.println("propagar de persona a persona.");
        System.out.println("El virus que causa el Covid-19 es un nuevo coronavirus que se");
        System.out.println("identificó por primera vez durante la investigación de un brote ");
        System.out.println("en Wuhan, China. En estos momentos, hay mas de 180, 000 infectados en todo el mundo.");
        
        } else if (menu==2){
        
        System.out.println("2.- Efectos/Sintomas");
        System.out.println("   ");
        System.out.println("Los signos y sintomas de la COVID-19, pueden aparecer entre 2 y 14 dias despues de");
        System.out.println("estar expuesto, y pueden incluir:");
        System.out.println("- Fiebre");
        System.out.println("- Tos");
        System.out.println("- Falta de aire o dificultad para respirar");
        System.out.println("  ");
        System.out.println("Otros sintomas pueden ser:");
        System.out.println("- Cansancio");
        System.out.println("- Dolores");
        System.out.println("- Goteo de nariz");
        System.out.println("- Dolor de garganta");
        System.out.println("  ");
        System.out.println("Causas");
        System.out.println("Se transmite de persona a persona entre aquellas que están en contacto cercano. Puede");
        System.out.println("contagiarse por gotitas respiratorias que se liberan cuando alguien con el virus tose");
        System.out.println("o estornuda. También se puede contagiar cuando una persona toca una superficie que");
        System.out.println("tiene el virus y luego se toca la boca, la nariz, o los ojos.");
        
        } else if (menu==3){
        
        System.out.println("3.- Poblacion vulnerable");
        System.out.println("   ");
        System.out.println("De acuerdo con la Secretaria de Salud federal, los adultos mayores, las mujeres");
        System.out.println("embarazadas, las personas inmunosuprimidas o quienes padecen enfermedades crónicas representan");
        System.out.println("el sector de la poblacion ma vulnerable frente a la nueva cepa del coronavirus COVID-19.");
        
        } else if (menu==4){
        
        System.out.println("4.- 3 Estados infectados");
        System.out.println("   ");
        System.out.println("Campeche: 0 ");
        System.out.println("Yucatán: 13 ");
        System.out.println("Quintana Roo: 6 ");
        
        } else if (menu==5){
        
        System.out.println("5.- Medidas de higiene");
        System.out.println("   ");
        System.out.println("Actualmente no existe vacuna para prevenir la COVID-19");
        System.out.println("Pero puede reducir el riesgo de infeccion:");
        System.out.println("- Lavandose las manos regularmente con agua y jabon o con desinfectante de manos a base de alcohol.");
        System.out.println("- Cubriendose la nariz y la boca al toser y estornudar con un pañuelo de papel desechable");
        System.out.println("o con la parte interna del codo.");
        System.out.println("- Evitando el contacto directo de 1 metro o 3 pies con cualquier persona con sintomas de resfriado o gripe");
        
        } else {
        System.out.println("No es un numero del 1 al 5");
        }
    }
    
    public static void main(String[] args) {
        // TODO code application logic here
 
        //Menu
        encabezado();
        separador (true);
        cuerpo (true);
        separador (true);
        codigo();
        separador (false);
             
  
    }
    
}
